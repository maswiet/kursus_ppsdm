# Kursus_PPSDM
![header_image](figures/Header_PPSDM.jpg)
# Half-day Online Course
## *"Prinsip Pemodelan Kedepan dan Inversi Data Geofisika"*

## **Instruktur:**  
Dr.rer.nat. Wiwit Suryanto, M.Si  
Dr. T Marwan Irnaka, M.Sc

## **Dipersiapkan oleh:**  

Prima Wira Kusuma Wardhani, M.Sc  
Anang Sahroni, S.Si  
Fittra Irwandhono, S.Si  

## **Waktu:**
Bandung 21 - 25 Maret 2022
Sesi 1: 21 Maret 2022

## **Tempat:**  
Pendidikan dan Pelatihan IP ASN Berbasis Online Learning

## **Agenda:**  
Memberikan wawawasan mengenai konsep dasar dan pengalaman praktikal mengenai pemodelan data geofisika dan aspek komputasinya 

## Luaran
1. Peserta dapat memahami fenomena dan teori prospekting geofisika dengan *ambient seismic noise*
2. Peserta dapat mengimplementasikan pengolahan *ambient seismic noise tomography* menggunakan Python dan Geopsy

## Peralatan dan kebutuhan course untuk peserta
1. Laptop ataupun *Personal Computer* (PC) yang terkoneksi dengan internet.  
2. Perangkat lunak Geopsy ([download Geopsy](https://www.geopsy.org/download/archives/geopsypack-win64-3.3.6.zip))
2. Akun Google (Gmail) untuk keperluan Google Colaboratory.

## Data:
1. Data seismik USArray TA (Transportable Array)
2. Koordinat stasiun untuk jaringan TA ([download](https://gitlab.com/geoseismal/ant_workshop/-/raw/feature/readme/data/TA2_coor.txt?inline=false))
3. Koordinat stasiun untuk Etna ([download](https://gitlab.com/geoseismal/ant_workshop/-/raw/feature/readme/data/Coor_EtnaINGV2.txt?inline=false))

## Jadwal

| Topik   |      Tautan      |
|----------|:-------------:|
| **SESI 1: 18 November 2021** |
| *Seismic Noise Prospecting Theory*|
| 1. Fenomena gelombang seismik: sumber, penjalaran, interaksi dengan medium, sensivitas instrumen | .. |
| 2. Paradigma seismik eksplorasi, dari metode aktif ke metode pasif | .. |
| 3. *State of the art* dari eksplorasi geofisika menggunakan derau seismik dari karakterisasi fitur dangkal ke karakterisasi fitur skala kerak dalam | .. |
||
| **SESI 2: 19 November 2021** |
| *Seismic Noise Prospecting Theory*|
| 1. Pengenalan bahasa pemrograman Python |  [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1HVfnaXa5I96OnG-e9sPC6dZLdOEaiw9x?usp=sharing) |
| 2. Pengenalan *library* yang digunakan untuk pengolahan | [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1PsiOYcaoprWAxVWw2Zhvg0tpVIC7dMjn?usp=sharing) |
| 3. Implementasi *Ambient Noise Tomography* di Python dan Geopsy | [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1oKmTczPq-VunrqsrVghEI8p75E9kzMQi?usp=sharing) |

## Perangkat lunak yang harus diinstall/disiapkan
1. **Geopsy versi 3.6.6**, adalah perangkat lunak *open source* untuk penelitian dan pengolahan data geofisika terutama yang kaitannya dengan data seismik. Geopsy akan digunakan pada proses inversi. ([download Geopsy](https://www.geopsy.org/download/archives/geopsypack-win64-3.3.6.zip))

Software yang dibutuhkan tersebut **sudah harus diinstall sebelum proses pemberian materi dimulai**.

## Bacaan Tambahan:
G. D. Bensen, M. H. Ritzwoller, M. P. Barmin, A. L. Levshin, F. Lin, M. P. Moschetti, N. M. Shapiro and Y. Yang, 2007, *Processing seismic ambient noise data to obtain reliable broad-band surface wave dispersion measurements*, Geophys. J. Int., 169, 1239–1260

## Lisensi
 [![CC BY 4.0][cc-by-shield]][cc-by]

Material ini di bawah lisensi [Creative Commons Attribution 4.0 International License][cc-by].

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

[1](https://colab.research.google.com/drive/1xIvTtYQmf6KQOydvxmhCStFpYdFcd38p?usp=sharing) - [2](https://colab.research.google.com/drive/1D7YDyhKWePwA3PV9vT_60d6nh9s6-RY-?usp=sharing) - [3](https://colab.research.google.com/drive/1uLRZYslcdI4zFpzPBuBPsSVQc0Mkw6xD?usp=sharing)
